
from bottle import route, static_file, run, get, response, request, put, HTTPResponse, BaseRequest
import json
import subprocess
from filehandler import JsonDAO, TarDAO, Filehandler

BaseRequest.MEMFILE_MAX = 10 * 1024 * 1024


def get_path():
    if check_if_in_docker():
        return "/test-reports/"
    else:
        return "test-reports/"

def check_if_in_docker():
    output = subprocess.check_output("cat /proc/1/sched | head -n 1", shell=True)
    in_a_host = output == 'init (1, #threads: 1)'
    return in_a_host


def allow_cors(func):
    def wrapper(*args, **kwargs):
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'
        return func(*args, **kwargs)
    return wrapper

def raise_error(msg, http_error_code):
    headers = {'Content-type': 'application/json'}
    response = {'status': 'failed', 'message': msg}
    json_response = json.dumps(response,headers)
    raise HTTPResponse(json_response, status=http_error_code, headers=headers)

path = get_path()
jsondao = JsonDAO(path)
tardao = TarDAO(path)
filehandler = Filehandler(tardao, jsondao)


@get('/reports')
@allow_cors
def return_json():
    json_file = filehandler.get_reports()
    if json_file == False:
        raise_error('Json file was not found!', 500)
    response.headers['Content-Type'] = 'application/json'
    return json.dumps(json_file)


@get('/reports/<report_id>') #Change this by usin tardao class' method open_tar
@allow_cors
def get_tar(report_id):
    path = get_path()
    return static_file(report_id+'.tar', root=path, mimetype='application/tar')


@put('/reports/<report_id>')
@allow_cors
def add_report(report_id):
    archive = request.body.getvalue()
    response_content = filehandler.add_report(report_id, archive)
    if response_content['Saved-report'] == False:
        raise_error('Json file was not found!', 500)
    response.headers['Content-Type'] = 'application/json'
    return json.dumps(response_content)


@route('/<:re:.*>', method='OPTIONS')
@allow_cors
def get_preflight():
    return 'ok'


if __name__ == '__main__':
    run(server='tornado', host='0.0.0.0', port=8090, debug=True)