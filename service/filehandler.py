import json
import os
import datetime
from functools import wraps


class JsonDAO(object):

    def __init__(self, path):
        self.path = path

    def update_json(self, json_file, item):
        if not self.update_existing_item(json_file, item):
            json_file.append(item)
            self.write_json(json_file)
        return item

    def update_existing_item(self, json_file, item):
        for index in range(len(json_file)):
            if json_file[index]["id"] == item["id"]:
                json_file[index] = item
                self.write_json(json_file)
                return item
        return False

    def delete_item(self, json_file, report_id):
        for index in range(len(json_file)):
            if json_file[index]["id"] == report_id:
                deleted_item = json_file[index]
                del json_file[index]
                self.write_json(json_file)
                return deleted_item
        return None

    def read_json(self):
        try:
            with open(self.path+'test-reports.json', 'r+') as file:
                data = json.load(file)
            return data
        except:
            return False

    def write_json(self, data):
        try:
            with open(self.path+'test-reports.json', 'w') as file:
                json.dump(data, file)
        except IOError:
            print('Can\'t write to file')


class TarDAO(object):

    def __init__(self, path):
        self.path = path

    def save_tar(self, report_id, data):
        try:
            with open(self.path+report_id+'.tar', 'w+') as file:
                file.write(data)
            return True
        except:
            return False

    def open_tar(self, report_id):
        try:
            with open(self.path+report_id+'.tar', 'r') as file:
                return file.read()
        except:
            return False

    def remove_tar(self, report_id):
        os.remove(self.path+report_id+'.tar')


#-----------Decorator for Fileshandler class-----------------------

def remove_outdated_files(func):
    @wraps(func)
    def inner_function(self, *args, **kwargs):

        time_now = datetime.datetime.now()
        json_file = self.jsondao.read_json()
        removed_items = []
        for item in json_file:
            run_time = datetime.datetime.strptime(item["time-stamp"], "%Y-%m-%d %H:%M:%S.%f")
            time_diff = time_now - run_time
            if time_diff.days > 7:
                self.jsondao.delete_item(json_file, item['id'])
                self.tardao.remove_tar(item['id'])
                removed_items.append(item)
        response_content = func(self, *args, **kwargs)
        response_content['Removed-items'] = removed_items
        return response_content
    return inner_function

class Filehandler(object):

    def __init__(self, tardao, jsondao):
        self.tardao = tardao
        self.jsondao = jsondao

    def get_reports(self):
        return self.jsondao.read_json()

    @remove_outdated_files
    def add_report(self, report_id, data):
        is_saved = self.tardao.save_tar(report_id, data)
        json_file = self.jsondao.read_json()
        new_item = {'id': report_id, 'time-stamp': str(datetime.datetime.now()), 'file-name': report_id+'.tar'}
        updated_item = self.jsondao.update_json(json_file, new_item) if json_file != False else False
        return {'Success': is_saved, 'Saved-report': updated_item}
