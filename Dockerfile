FROM alpine:3.5

RUN apk update
RUN apk add --no-cache \
    python \
    py-pip \
    curl


RUN pip install --upgrade pip

RUN pip install bottle \
    tornado
RUN pip install requests

EXPOSE 8090
COPY service/* /service/
RUN mkdir /test-reports
COPY test-reports/* /test-reports/
#ENTRYPOINT ["/usr/bin/python"]


CMD ["python","/service/service.py"]