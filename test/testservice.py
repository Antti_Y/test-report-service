import unittest
import requests
import json
import datetime

BASE_URL = 'http://localhost:8090/reports'
class TestService(unittest.TestCase):


    def test_add_report(self):

        response = requests.put(BASE_URL + '/kala', data=b'kala')
        self.assertEqual(response.status_code, 200)
        saved_report = response.json()['Saved-report']
        self.assertEqual(saved_report['id'], 'kala')


    def test_get_json(self):

        response = requests.get(BASE_URL)
        reports_summary = response.json()
        self.assertEqual(response.status_code, 200)
        print(reports_summary)
        self.assertEqual(len(reports_summary), 4)

    def test_get_tar(self):

        response = requests.get(BASE_URL + '/kala')
        self.assertEqual(response.status_code, 200)
        raw_data = response.content
        print(raw_data)
        self.assertEqual(raw_data, "kala")


    def test_preflight(self):
        headers = {'Origin': 'http://www.google.fi'}

        response = requests.options(BASE_URL)
        self.assertEqual(response.status_code, 200)



if __name__ == '__main__':
    unittest.main()